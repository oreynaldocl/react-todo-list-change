import React from 'react';
import { func, number } from 'prop-types';
import { Header } from 'semantic-ui-react'

TodoTitle.propTypes = {
  count: number.isRequired,
  checked: number.isRequired,
  onColorChange: func,
};

// Explanation default values http://es6-features.org/#DefaultParameterValues
function TodoTitle({ count = 0, checked = 0 }) {
  return (
    <Header as="h2">
      Todo List {count > 0 && renderCount(count, checked)}
    </Header>
  );
}

function renderCount(count, checked) {
  return (
    <span>
      [
        <span title="Checked">{checked}</span> /
        <span title="Total">{count}</span>
      ]
    </span>
  );
}

export default TodoTitle;
