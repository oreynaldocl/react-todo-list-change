This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Table of Contents
- [Running Application](#running-application)
- [Steps To Configure Initial App](#steps-to-configure-initial-app)

## Running Application

Execute the following line:
```sh
npm install
npm start
```

## Steps To Configure Initial App

For creating the app from scratch first install node js. In preference Ubuntu or linux distribution it is easy than install in Windows.

### Install Node JS

You can install with official steps, but I prefer to install Node JS using NVM, because you can have different versions of node js: https://github.com/creationix/nvm

### Install create-react-app

Install create-react-app package: https://github.com/facebookincubator/create-react-app

```sh
npm install -g create-react-app
```

### Create react app

```sh
create-react-app demo-todo-list
cd demo-todo-list
npm install
npm install semantic-ui-react semantic-ui-css --save # Update index.js folowing semantic documentation https://react.semantic-ui.com/usage
```

Play with React